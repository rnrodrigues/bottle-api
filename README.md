Bottle Api
==========

Example using [Bottle python framework](http://bottlepy.org/) to build APIs with [Ember.js](http://emberjs.com/) or other javascript framework from your choice.

Instalation
-----------

- Install [Virtualenv](https://virtualenv.pypa.io/en/latest/).
- Create a virtual environment and change to it.
- Clone this repository and change to "bottle-api" folder.
- Install requirements with pip install -r requirements.txt.

There are two python files: one use [Gevent](http://gevent.org) (app.py) and other use [Tornado](http://tornadoweb.org).

Thats it and enjoy!