from webtest import TestApp
import app


def test_funcional_index():
    testapp = TestApp(app.application)
    assert testapp.get('/').status == "200 OK"


def test_funcional_products():
    testapp = TestApp(app.application)
    assert testapp.get('/api/v1/products').status == "200 OK"
