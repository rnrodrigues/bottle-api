App = Ember.Application.create();

App.Router.map(function() {
  // put your routes here
});

App.Product = DS.Model.extend({
  name: DS.attr()
});

App.ApplicationAdapter = DS.RESTAdapter.extend({
    namespace: 'api/v1'
});

App.IndexRoute = Ember.Route.extend({
  model: function() {
    return this.get('store').find('product');
  }
});
