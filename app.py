#!/usr/bin/env python
# -*- encoding: utf-8 -*-
from gevent import monkey
monkey.patch_all()
from bottle import app, route, run, static_file, response
import os
import json


root = os.path.join(os.path.dirname(__file__), 'public')
application = app = app()


def jsonify(*args, **kwargs):
    response.set_header('Content-Type', 'application/json; charset=utf-8')
    return json.dumps(dict(*args, **kwargs), separators=(",", ":"),
                      ensure_ascii=False)


@app.route('/<path:path>')
def serve_files(path):
    yield static_file(path, root)


@app.route('/')
def index():
    yield static_file("index.html", root)


@app.route('/api/v1/products', method="GET")
def get_products():
    yield jsonify(
        {
            "products":
                [
                    {"id": "1", "name": "Coffee"},
                    {"id": "2", "name": "Milk"},
                    {"id": "4", "name": "Cheese"},
                    {"id": "8", "name": "Sugar"},
                    {"id": "9", "name": "Bread"}
                ]
        }
    )


@app.route('/api/v1/states', method="GET")
def get_products():
    yield jsonify(
        {
            "states":
                [
                    {"id": "1", "name": "Alagoas"},
                    {"id": "2", "name": "Acre"},
                    {"id": "4", "name": "Amapá"},
                    {"id": "8", "name": "Amazonas"},
                    {"id": "9", "name": "Bahia"}
                ]
        }
    )


if __name__ == "__main__":
    run(host="0.0.0.0", port=int(os.environ.get("PORT", 3000)),
        server="gevent", quiet=True)
